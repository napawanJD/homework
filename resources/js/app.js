
require('./bootstrap');

window.Vue = require('vue');


import Vue from 'vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
library.add(fas);

import { BootstrapVue, 
        IconsPlugin,
        BNavItem,
        NavPlugin,
        CardPlugin,
        BCardGroup,
        BIcon
    } from 'bootstrap-vue'

Vue.use(BootstrapVue,
        IconsPlugin, 
        NavPlugin,
        CardPlugin, 
    );

Vue.component('b-icon', BIcon);
Vue.component('b-nav-item', BNavItem);
Vue.component('b-card-group', BCardGroup);
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component('show-list', require('./components/ShowlistComponent.vue').default);
Vue.component('layout-component', require('./components/LayoutComponent.vue').default);


const app = new Vue({
    el: '#app',
});
